package com.example.demo.service.impl;

import com.example.demo.repository.AppoinmentRepository;
import com.example.demo.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    private AppoinmentRepository appRepo;

    @Autowired
    public AppointmentServiceImpl(AppoinmentRepository appRepo){
        this.appRepo = appRepo;
    }

    @Override
    public String deleteAppointment(Long id) {
        String response = null;

        try{
            appRepo.deleteById(id);
            response = "Appointment delete successful";
        }
        catch(Exception e){
            response = "Appointment delete not successful";
        }
        return null;
    }
}
