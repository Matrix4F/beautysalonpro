package com.example.demo.service.impl;

import com.example.demo.controller.SendMailGApi;
import com.example.demo.model.Appoinment;
import com.example.demo.model.PasswordResetToken;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.*;
import com.example.demo.service.UserService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private UserRepo userRepo;
    private AppoinmentRepository appRepo;
    private PasswordResetTokenRepo passwordResetTokenRepo;

    SendMailGApi sendMailGApi;
    PasswordResetToken passwordResetToken;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,RoleRepository roleRepository,UserRepo userRepo,AppoinmentRepository appRepo
    ,PasswordResetTokenRepo passwordResetTokenRepo){
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userRepo = userRepo;
        this.appRepo = appRepo;
        this.passwordResetTokenRepo = passwordResetTokenRepo;
    }

    @Override
    public List<User> userList() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findUser(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public String addUser(User user) {

        //user.setRole(roleRepository.findById(new Long(1)));
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        Optional<Role> role = roleRepository.findById(new Long(2));
        user.setRole(role.get());

        userRepo.save(user);


        /*try {
            *//*if(user.getId() == null){
                message = "Added";
            }
            else{
                message ="Updated";
            }*//*
            //user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            //userRepository.save(user).getUserName();
            //Optional<Role> role = roleRepository.findById(user.getRoleId());
            //user.setRole(role.get());
            //jsonObject.put("title" , message+" Confirmation");
            //jsonObject.put("message",userRepository.save(user).getUserName()+" " +message+" successfully.");
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        return "Hi";
    }

    @Override
    public String addAppoinment(Appoinment appoinment) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        appoinment.setUser(findByUserName(name));
        appRepo.save(appoinment);
        return "Appoinment added succsessfuly";
    }

    @Override
    public String approveAppoinment(Appoinment appoinment) {
        appRepo.save(appoinment);
        return "Approve  succsessfull";
    }

    @Override
    public String deleteUser(Long id) {
        String resualt = null;
        try{
            userRepository.deleteById(id);
            resualt = "User Delete Successful";
        }
        catch (Exception e) {
             e.printStackTrace();
        }
        return resualt;
    }

    @Override
    public List<Role> roleList() {
        return roleRepository.findAll();
    }

    @Override
    public User findByUserName(String userName) {
        User user = userRepository.findByUserName(userName);
        return user;
    }

    @Override
    public String genaratePasswordResetToken(String username,HttpServletRequest request) {

        String server = request.getServerName();
        String port = String.valueOf(request.getServerPort());


        String returnMessage = null;

        sendMailGApi = new SendMailGApi();

        String token = UUID.randomUUID().toString();
        User user = userRepository.findByUserName(username);

        /*String url = "http://localhost:9092/beautysalon/emailVerification/"+user.getId()+"/"+token;*/

        String url = "http://"+server+":9092/beautysalon/emailVerification/"+user.getId()+"/"+token;

        passwordResetToken = new PasswordResetToken(token,user);
        passwordResetTokenRepo.save(passwordResetToken);

        user.setPasswordResetToken(passwordResetToken);
        userRepository.save(user);
        try{
           sendMailGApi.sendFromGMail(url);
            returnMessage = "password reset details send to your gmail";
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return returnMessage;
    }
}
