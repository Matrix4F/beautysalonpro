package com.example.demo.repository;

import com.example.demo.model.PasswordResetToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PasswordResetTokenRepo extends JpaRepository<PasswordResetToken,Long> {

    @Query("from PasswordResetToken where token = :token")
    PasswordResetToken findByToken(@Param("token") String token);

    @Query("from PasswordResetToken where id = :id")
    PasswordResetToken findByTokenId(@Param("id") Long id);

}
