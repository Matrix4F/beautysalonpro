package com.example.demo.repository;

import com.example.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User,Long> {

    @Query("from User where user_name=:username")
    User findByUserName(@Param("username") String username);

    @Query("from User where id=:id")
    User findByUserId(@Param("id") Long id);




}
