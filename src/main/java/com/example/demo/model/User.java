package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Set;

@Entity
public class User extends AbstractPersistable<Long> {

    private String userName;
    private String email;
    private String phoneNo;
    private String password;

    private transient Long id;
    /*private transient Long roleId;*/

    @ManyToOne
    @JoinColumn(name ="role_id")
    private Role role;

    @OneToOne
    @JoinColumn(name="token_id")
    private PasswordResetToken passwordResetToken;

    @OneToMany(targetEntity = Appoinment.class ,mappedBy = "user",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Set<Appoinment> appoinments;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public PasswordResetToken getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(PasswordResetToken passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", password='" + password + '\'' +
                ", id=" + id +
                ", role=" + role +
                ", passwordResetToken=" + passwordResetToken +
                ", appoinments=" + appoinments +
                '}';
    }
}
