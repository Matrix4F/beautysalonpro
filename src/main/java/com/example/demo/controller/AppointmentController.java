package com.example.demo.controller;

import com.example.demo.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RestController
public class AppointmentController {

    private AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService){
        this.appointmentService = appointmentService;
    }

    @RequestMapping(value = "/main/deleteAppointment/{id}")
    public String deleteAppoinment(@PathVariable Long id){
        String response = null;
        response = appointmentService.deleteAppointment(id);
        return response;
    }
}
