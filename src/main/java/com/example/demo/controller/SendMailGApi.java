package com.example.demo.controller;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.UUID;

public class SendMailGApi {

    private static String USER_NAME = "kenonlanka@gmail.com";  // GMail user name (just the part before "@gmail.com")
    private static String PASSWORD = "kenonlanka@1234"; // GMail password
    private static String RECIPIENT = "kdpsamaraweera@gmail.com";

    /*public static void main(String[] args) {
        *//*String token = UUID.randomUUID().toString();
        System.out.println(token);*//*
        String from = USER_NAME;
        String pass = PASSWORD;
        String[] to = { RECIPIENT }; // list of recipient email addresses
        String subject = "Java send mail example";
        String body = "Welcome to JavaMailRRR!";

        sendFromGMail(from, pass, to, subject, body);
    }*/

    public  void sendFromGMail(String url) {

        String from = "kenonlanka@gmail.com";
        String pass = "kenonlanka@1234";
        String[] to = { "kdpsamaraweera@gmail.com "}; // list of recipient email addresses
        String subject = "Java send mail example";
        String body = "Welcome to JavaMailRRR!";

        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        /*Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USER_NAME, PASSWORD);
            }
        });*/
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for( int i = 0; i < to.length; i++ ) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            /*message.setText(body);*/
            message.setContent(
                    "\"<h1>This is actual message embedded in \n" +
                            url +
                            "   HTML tags</h1>","text/html; charset=utf-8"
            );
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (AddressException ae) {
            ae.printStackTrace();
            System.out.println("========================"+ae);
        }
        catch (MessagingException me) {
            System.out.println("========================"+me);
            me.printStackTrace();
        }
    }
}
