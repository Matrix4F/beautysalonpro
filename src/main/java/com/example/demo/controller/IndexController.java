package com.example.demo.controller;

import com.example.demo.model.Appoinment;
import com.example.demo.model.PasswordResetToken;
import com.example.demo.model.User;
import com.example.demo.repository.AppoinmentRepository;
import com.example.demo.repository.PasswordResetTokenRepo;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.apache.struts.mock.MockHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

@RestController
public class IndexController implements ErrorController {

    private UserRepository userRepository;
    private UserService userService;
    private AppoinmentRepository appoinmentRepository;
    private PasswordResetTokenRepo passwordResetTokenRepo;

    RedirectView redirectView = null;
    PasswordResetToken passwordResetToken = null;

    @Autowired
    public IndexController(UserService userService,UserRepository userRepository,AppoinmentRepository appoinmentRepository
        ,PasswordResetTokenRepo passwordResetTokenRepo) {

        this.userService = userService;
        this.userRepository = userRepository;
        this.appoinmentRepository = appoinmentRepository;
        this.passwordResetTokenRepo = passwordResetTokenRepo;
    }
    private static final String PATH ="/error";

    @Override
    public String getErrorPath() {
        return PATH;
    }

    @RequestMapping(PATH)
    public String error(){
        return "No Mapping Available";
    }

    @RequestMapping(value = "/main/allCustomerDetails")
    public List<User> allCustomerDetails() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        List<User> customers = userRepository.findAll();
        return customers;
    }

    @RequestMapping(value = "/{subparam}/getAllAppoinmentDetails")
    public List<Appoinment> getAllAppoinmentDetails(){
        List<Appoinment> appoinment = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        User user = userRepository.findByUserName(name);
        Long id = user.getId();
        Long roleId = user.getRole().getId();

        //userService.genaratePasswordResetToken("qwerd",user);

        try {
            if(roleId==1){
                appoinment = appoinmentRepository.findAll();
            }
            else if(roleId==2){
                appoinment = appoinmentRepository.findAppoinmentByUserId(id);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return appoinment;
    }
    /*@RequestMapping(value = "/mainforAppoinment/getAllAppoinmentDetails")
    public List<Appoinment> getAllAppoinmentDetails4SlideBar(){
        List<Appoinment> appoinment = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        User user = userRepository.findByUserName(name);
        Long id = user.getId();
        Long roleId = user.getRole().getId();

        //userService.genaratePasswordResetToken("qwerd",user);

        try {
            if(roleId==1){
                appoinment = appoinmentRepository.findAll();
            }
            else if(roleId==2){
                appoinment = appoinmentRepository.findAppoinmentByUserId(id);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return appoinment;
    }*/

    @RequestMapping(value = "/main/getAllPendingAppoinmentDetails/{apporpend}")
    public List<Appoinment> getAllPendingAppoinmentDetails(@PathVariable String apporpend){
        List<Appoinment> appoinment = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        User user = userRepository.findByUserName(name);
        Long id = user.getId();
        Long roleId = user.getRole().getId();
        String pendingApprovalId ;
        if(apporpend.equals("pending")) {
            pendingApprovalId = "0";
        }
        else{
            pendingApprovalId = "1";
        }
        try {

                appoinment = appoinmentRepository.findPendingAppoinment(pendingApprovalId);

        }catch(Exception e){
            e.printStackTrace();
        }
        return appoinment;
    }

    @RequestMapping(value = "/regForm/postcustomer", method = RequestMethod.POST)
    public void postCustomer(@RequestBody User user) {
        userService.addUser(user);

    }

    @RequestMapping(value = "/regForm/getUserByUserName/{username}",method = RequestMethod.GET)
    public String getUserByUserName(@PathVariable String username) {
        String existDbUsername;
        String output = null;
        try {
            existDbUsername = userRepository.findByUserName(username).getUserName();
            output = "exist";

        }
        catch (Exception e){
            output = "notexist";
        }
        System.out.println("======================="+output);
        return  output;

    }

    @RequestMapping(value = "/main/addAppoinment")
    public void postAppoinment(@RequestBody Appoinment appoinment){
        appoinment.setApproval("0");
        userService.addAppoinment(appoinment);
    }

    @RequestMapping(value = "/main/doApproval/{appoinmentId}")
    public void doApproval(@PathVariable Long appoinmentId){
        Appoinment appoinment = new Appoinment();
        try{
            appoinment = appoinmentRepository.findAppoinmentByAppoinmentId(appoinmentId);
            appoinment.setApproval("1");
            appoinment.setUser(appoinment.getUser());
            userService.approveAppoinment(appoinment);
        }
        catch (Exception e){
            System.out.println(e);
        }

    }

    @RequestMapping(value = "/main/deleteUser/{id}")
    public String userDetails(@PathVariable Long id){


       // System.out.println(userService.deleteUser(id));
        String response = null;
        response = userService.deleteUser(id);
        return response;

        //Worked code
        /*Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        User user = userRepository.findByUserName(name);
        Long userId = user.getId();
        userService.deleteUser(userId);*/

    }
    @RequestMapping(value = "/forgetPassword/sendMail/{username}")
    public String sendMail(@PathVariable String username,Model model){
        String message = null;
        MockHttpServletRequest request = new MockHttpServletRequest();

        try {
            message = userService.genaratePasswordResetToken(username,request);

        }
        catch (Exception e){
            message = "User name not exist";
        }
        return message;
    }

    @RequestMapping(value = "/emailVerification/{id}/{token}")
    public RedirectView emailVerification(@PathVariable Long id,@PathVariable String token,Model model,HttpServletRequest request, HttpServletResponse response){
        System.out.println("=====================/emailVerification/{id}/{token}");

        String message =null;
        ModelAndView mav = null;
        RedirectView redirectView = null;
        String stringId = Long.toString(id);

        try {
            PasswordResetToken passwordResetToken = passwordResetTokenRepo.findByToken(token);

            System.out.println("=======%%%%%%%%%%%%======= "+passwordResetToken.getToken());
            if (passwordResetToken != null && passwordResetToken.getUser().getId() == id) {

                Calendar cal = Calendar.getInstance();

                if ((passwordResetToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
                    message = "Your Reset password Link has been Exipired request";
                    System.out.println("================= Time is Not OK");
                }

                else{
                    System.out.println("Your email has been verified");
                    //mav = new ModelAndView("forget_password");
                    redirectView = new RedirectView();
                    redirectView.setUrl("http://localhost:9092/beautysalon/resetPassword");
                    model.addAttribute("user","welcome future");

                    HttpSession session = request.getSession(true);
                    session.setAttribute("userId",stringId);

                }
            }
        }
        catch (Exception e){

        }

        return redirectView;
    }

    @RequestMapping(value="/resetPassword/{userId}/{password}")
    public RedirectView updatePassword(@PathVariable String userId, @PathVariable String password){

        redirectView = new RedirectView();
        passwordResetToken = new PasswordResetToken();

        User user = new User();
        Long id = Long.valueOf(userId).longValue();
        try{
            user = userRepository.findByUserId(id);

            user.setPassword(new BCryptPasswordEncoder().encode(password));
            userRepository.save(user);

            passwordResetToken = passwordResetTokenRepo.findByTokenId(user.getPasswordResetToken().getId());
            passwordResetToken.setToken("0");
            passwordResetTokenRepo.save(passwordResetToken);

            redirectView.setUrl("http://localhost:9092/beautysalon/login");
        }
        catch(Exception e){
            e.printStackTrace();
        }


        return redirectView;
    }

}
