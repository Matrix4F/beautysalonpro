package com.example.demo.controller;

import com.example.demo.Response;
import com.example.demo.model.Appoinment;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller
public class MainController {

    private UserRepository userRepository;
    private UserService userService;

    @Autowired
    public MainController(UserService userService,UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @RequestMapping("/main2")
    public String main2(){
        return "main2";
    }

    @RequestMapping("/beautiworld")
    public String home(){
        return "index";
    }

    @RequestMapping("/usersdetails")
    public String usersDet(){
        return "userdetails";
    }

    @RequestMapping("/regForm")
    public String regPage2(){
        return "reg";
    }

    @RequestMapping("/appoinment")
    public String welcome(){
        return "appoinment";
    }

    @RequestMapping("/addAppoinment")
    public String addAppoinment(){
        return "add_appoinment";
    }

    @RequestMapping("/forgetPassword")
    public String forgetPassword(Model model){
        //model.addAttribute("message","You have been logout successfully");
        return "forget_password";
    }

    @RequestMapping("/resetPassword")
    public String resetPassword(Model model){
        //model.addAttribute("message","You have been logout successfully");

        return "reset_password";
    }



    /*@RequestMapping("/login")
    public String login(){
        return "login";
    }*/

    @GetMapping("/login")
    public String login(Model model,String error,String logout){
        if(error != null){
            model.addAttribute("error","Your username and password is invalid");
        }
        if(logout != null){
            model.addAttribute("message","You have been logout successfully");

        }
        return "login";
    }

    @RequestMapping("/{pathValue}") // Call http://localhost:9092/beautysalon/main ,http://localhost:9092/beautysalon/mainforAppoinment,http://localhost:9092/beautysalon/mainforUserDetails
    public String mainPage(Model model,@PathVariable String  pathValue){

        String modelValue = null;

        if(pathValue.equals("main")||pathValue.equals("mainforAppoinment")){
            modelValue = "appoinment";
        }
        else if(pathValue.equals("mainforUserDetails")){
            modelValue = "userdetails";
        }

        model.addAttribute("message",modelValue);
        return "main";
    }

    /*@RequestMapping("/mainforUserDetails")
    public String mainPageforUserDetails(Model model){
        model.addAttribute("message","userdetails");
        return "main";
    }

    @RequestMapping("/mainforAppoinment")
    public String mainPageforAppoinment(Model model){
        model.addAttribute("message","appoinment");
        return "main";
    }*/




    /*@RequestMapping(value = "/addAppoinment/addAppoinment")
    public void postAppoinment(@RequestBody Appoinment appoinment){
        userService.addAppoinment(appoinment);
    }*/

    /* @RequestMapping(value = "/regForm/postcustomer", method = RequestMethod.POST)
    public void postCustomer(@RequestBody User user) {
        userService.addUser(user);

    }*/
    /*@RequestMapping(value = "/appoinment/allCustomerDetails")
    public List<User> allCustomerDetails() {
        *//*If doesnt work then directly call userReopository.findall() method*//*
        *//*Iterable<User> customers = userService.userList();
        return new Response("Done",customers);*//*

        List<User> customers = userRepository.findAll();
        System.out.println(customers.get(0).getUserName());
        customers.get(1).getUserName();
        //return new Response("Done",customers);
        return customers;
    }*/

    /*@RequestMapping(value = "/registration",method = RequestMethod.POST)
    public  String addUser(@ModelAttribute("userSubmit") User user, BindingResult result, ModelMap model) {
        User usr = user;
        String name = usr.getUserName();
        userService.addUser(user);
        return "cc";
    }
    @RequestMapping(value="/regform",method=RequestMethod.POST)
    public String saveMessage(@ModelAttribute("userSubmit") User user, BindingResult result, ModelMap model) {
        String s = user.getUserName();
        return "homepage";
    }
    @GetMapping("/form")
    public String userForm(Model model) {
        model.addAttribute("userForm",new User());
        return "/form";
    }
    @RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
    public String submit(@Valid @ModelAttribute("userForm")User user,
                         BindingResult result, ModelMap model) {

        userService.addUser(user);
        return "Success";

    }*/
}
