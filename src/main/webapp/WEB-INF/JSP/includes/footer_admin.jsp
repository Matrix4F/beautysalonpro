<%-- 
    Document   : footer_admin
    Created on : Oct 5, 2016, 2:18:50 PM
    Author     : nuwan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--Administrator footer-->
<footer class="site-footer" style="text-align: center; background-color: #fffffc">
    <div class="site-footer-center">
        <p><span class="site-footer-legal">BeautySalon © 2018. All RIGHT RESERVED.</span></p>
        <p><span class="site-footer-legal">Solution by <a href="/">Matrix</a></span></p>
    </div>
</footer>
<!--End footer-->

