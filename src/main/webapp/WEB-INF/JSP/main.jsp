<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var ="path" value="${pageContext.request.contextPath}"></c:set>

<%@include file="includes/header_admin.jsp"%>
<%@include file="includes/side_navibar.jsp" %>

<body>
<div class="page animsition">
    <div id="main_page">

        <%@include file="appoinment.jsp"%>
        <%--<%@include file="userdetails.jsp" %>--%>

    </div>
</div>
<%@include file="includes/footer_admin.jsp" %>

</body>
</html>

