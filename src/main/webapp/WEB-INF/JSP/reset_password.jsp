<%@ page import="com.example.demo.model.User" %>
<%@ page language="java" import="java.sql.*" %>
<%@page import="java.lang.Object" %>
<%@page import="java.lang.String"%>
<%--
Created by IntelliJ IDEA.
User: Nuwan
Date: 7/2/2018
Time: 1:58 PM
To change this template use File | Settings | File Templates.
--%>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var ="path" value="${pageContext.request.contextPath}"></c:set>

<!------ Include the above in your HEAD tag ---------->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
    <title>Login and Registration Form with HTML5 and CSS3</title>
    <script
            src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script>
    <script type="text/javascript" src="${path}/js/angularCode.js"></script>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/logincss.css" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

</head>
<body>
<div class="container" ng-app="app">

    <header>
        <h1>Beauty Salon</h1>

    </header>
    <section>
        <div id="container_demo" style="float: right; margin-right: 100px">
            <div id="wrapper">
                <div  ng-controller="userRegController"  class="animate form">

                    <form   ng-submit="updatePassword('<%= session.getAttribute("userId") %>')"  autocomplete="on">
                        <h1> Reset Password </h1>

                        <div ng-if="passwordConfMsg != null">
                            <div class ="alert alert-success">
                                <strong><span ng-bind="passwordConfMsg"></span></strong>
                            </div>
                        </div>
                        <p>
                            <label class="youpasswd" >Password </label>
                            <input type="password" name="password" ng-model="password" required="required" ng-model="password"/>
                        </p>
                        <p>
                            <label  class="youpasswd" >Please confirm your password </label>
                            <input  type="password" name="passwordsignup_confirm" required="required" ng-model="passwordsignup_confirm" ng-change="passwordConfirmation(password)" ng-Trim="false"  />
                        </p>
                        <%--<br /><span ng-bind="passwordConfMsg"></span>--%>

                        <p class="signin button" >
                            <input  ng-disabled="disabled" type="submit"  value="Submit"/>
                        </p>
                    </form>

                </div>
            </div>
        </div>
    </section>

</div>
</body>
</html>






