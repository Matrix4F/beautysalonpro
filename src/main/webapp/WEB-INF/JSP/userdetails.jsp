<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var ="path" value="${pageContext.request.contextPath}"></c:set>

<%--<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>--%>
<%@include file="includes/includejs.jsp" %>
<script type="text/javascript" src="${path}/js/angularCode.js"></script>
<script type="text/javascript" src="${path}/js/styleAll.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">


<body>


<div class="page-header" >
    <h1 class="page-title">Customer Details</h1>
    <ol class="breadcrumb">
        <li><a href="">Dashboard</a></li>
        <li class="active">Customer Details</li>

    </ol>
    <div class="page-header-actions">

    </div>
</div>


<div class="page-content" ng-app="app">
    <div class="panel">
        <div ng-controller="allCustomerDetails" class="panel-body" ng-init="showData()">
            <header class="panel-heading">
                <div class="row">

                    <div class="col-md-4">
                        <h4 class="panel-title">Customer Details &nbsp;<span class="tooltip-dark" data-toggle="tooltip" data-original-title="Help tips" data-trigger="focus / hover"><button type="button" class="btn-info popover-info btn-sm btn-icon btn-inverse btn-info btn-round" data-content="Can accept or reject any applications for selection test in this table. And can search any content of the table. And also can view fully application through the id." data-toggle="popover" data-original-title="About this table..." data-trigger="click"><i class="grey-500 icon wb-info"></i></button></span></h4>
                    </div>
                    <div class="col-md-8" >
                        <br/>
                        <table>
                            <td>
                                <div class="col-md-12">
                                    <input class="form-control" type="text" ng-model="searchText" placeholder="Search by anything">
                                </div>
                            </td>
                            <td>
                                <div class="col-md-12">
                                    <a href="main2"><input type="button" class="btn btn-primary" value="Add Customer"/></a>
                                </div>
                            </td>

                        </table>
                    </div>

                </div>
            </header>
            <div style="text-align: center">



            </div>
            <table  class="table table-hover dataTable table-striped width-full table-bordered">
                <thead>
                <th>UserId</th>
                <th>UserName</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Edit</th>
                <th>Delete</th>
                </thead>

                <tr ng-repeat="customer in listCustomers | filter:searchText | pagination: curPage * pageSize | limitTo: pageSize">

                    <td >{{$index + 1 }}</td>
                    <td >{{customer.userName}}</td>
                    <td >{{customer.email}}</td>
                    <td >{{customer.phoneNo}}</td>

                    <td >

                        <button type="button" class="btn btn-default btn-xs"
                                data-toggle="modal" data-target="#editCustomer{{$index}}" ><span class="glyphicon glyphicon-trash"></span>Edit</button>

                        <div   class="modal fade" id="editCustomer{{$index}}" tabindex="-1" role="dialog" aria-labelledby="executionOptionLabel"
                               aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <br>
                                    <h3 align="center">Edit Customer</h3>
                                    <div class="modal-body">



                                        <div class="form-group">
                                            <label>Customer Name</label>
                                            <input class="form-control" id="cusname" name="cusname" value="{{customer.userName}}"></input>
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" id="email" name="email" value="{{customer.email}}"></input>
                                        </div>
                                        <div class="form-group">
                                            <label>Phone No</label>
                                            <input class="form-control" id="phoneNo" name="phoneNo" value="{{customer.phoneNo}}"></input>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <button type="submit" class="col-md-2 col-sm-offset-10 btn btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </td>

                    <%--<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-sm" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span>Delete</button></p></td>--%>
                    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ng-click="deleteCustomer($index)"><span class="glyphicon glyphicon-trash"></span>Delete</button></p></td>
                </tr>
                </tbody>

            </table>
            <br/>

            <div style="float: right" class="pagination pagination-centered" ng-show="listCustomers.length">
                <ul class="pagination-controle pagination">
                    <li>
                        <button type="button" class="btn btn-primary" ng-disabled="curPage == 0"
                                ng-click="curPage=curPage-1"> &lt;</button>
                    </li>
                    <li>
                        <button class="btn" readonly="true">{{curPage + 1}} of {{ numberOfPages() }}</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-primary"
                                ng-disabled="curPage >= listCustomers.length/pageSize - 1"
                                ng-click="curPage = curPage+1">&gt;</button>
                    </li>
                </ul>
            </div>

        </div>

    </div>

</div>


<!-- End Page -->

<!-- Footer -->


<script>
    $('#check-out').datepicker();
</script>


<script>
    (function (document, window, $) {
        'use strict';

        var Site = window.Site;

        $(document).ready(function ($) {
            Site.run();
        });

    })(document, window, jQuery);
</script>

</body>

</html>