<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var ="path" value="${pageContext.request.contextPath}"></c:set>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>--%>
<%@include file="includes/includejs.jsp" %>
<script type="text/javascript" src="${path}/js/angularCode.js"></script>
<script type="text/javascript" src="${path}/js/styleAll.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">


<style>




</style>

<body>


<%--<div class="page-header" >
    <h1 class="page-title">Appointment</h1>
    <ol class="breadcrumb">
        <li><a href="">Dashboard</a></li>
        <li class="active">Appointment</li>

    </ol>

    <div class="page-header-actions">

    </div>
</div>--%>


<div class="page-content" id ="full_page" ng-app="app">
    <div class="panel" id="appoid">
        <div ng-controller="allAppoinmentDetails" class="panel-body" ng-init="showData()">
            <header class="panel-heading">
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="panel-title">Appointment<span class="tooltip-dark" data-toggle="tooltip" data-original-title="Help tips" data-trigger="focus / hover"><button type="button" class="btn-info popover-info btn-sm btn-icon btn-inverse btn-info btn-round" data-content="Can accept or reject any applications for selection test in this table. And can search any content of the table. And also can view fully application through the id." data-toggle="popover" data-original-title="About this table..." data-trigger="click"><i class="grey-500 icon wb-info"></i></button></span></h4>
                    </div>
                    <%--<input id="dateValueX" name="dateXX" ng-model="dateXX" type="text" class="form-control" data-plugin="datepicker"/>--%>
                    <%--<Add apointment button>--%>


                    <div class="col-md-8" >
                        <br/>
                        <table>
                            <td>
                                <div class="col-md-12">
                                    <input class="form-control" type="text" ng-model="searchText" placeholder="Search by anything">
                                </div>
                            </td>
                            <td>
                                <div class="col-md-12">
                                    <a id="addAppoinment"><input type="button" class="btn btn-primary" value="New Appoinment"/></a>
                                </div>
                            </td>
                            <td>
                                <div class="col-md-12">
                                    <input  type="button" ng-click="pendingAction('approved')" class="btn btn-primary" value="Approval"/>
                                </div>
                            </td>
                            <td>
                                <div class="col-md-12">
                                    <input type="button" ng-click="pendingAction('pending')" class="btn btn-primary" value="Pending"/>
                                </div>
                            </td>
                            <td>
                                <div class="col-md-12">
                                    <input type="button" ng-click="pendingAction('all')" class="btn btn-primary" value="All"/>
                                </div>
                            </td>



                        </table>
                    </div>

                </div>
            </header>
            <div style="text-align: center">



            </div>
            <table  class="table table-hover dataTable table-striped width-full table-bordered">
                <thead>
                <th>Type</th>
                <th>Date</th>
                <th>Time</th>
                <th>Description</th>
                <th>Approve</th>
                <th>Edit</th>
                <th>Delete</th>
                </thead>




                <tr ng-repeat="appoinment in listAppoinment | filter:searchText | pagination: curPage * pageSize | limitTo: pageSize">
                    <c:set var = "salary"  value = "{{appoinment.date}}"/>

                    <td>{{appoinment.type}}</td>
                    <td>{{appoinment.date}}</td>
                    <td>{{appoinment.time}}</td>


                    <td>
                        <button type="button" class="btn btn-default btn-sm"
                                data-toggle="modal" data-target="#dialogTestDialog{{$index}}" >Description</button>

                        <div   class="modal fade" id="dialogTestDialog{{$index}}" tabindex="-1" role="dialog" aria-labelledby="executionOptionLabel"
                               aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <br>
                                    <h3 align="center">Detail on Appoinment</h3>
                                    <div class="modal-body">
                                        <p>{{appoinment.date}}</p>
                                        <p>{{appoinment.time}}</p>
                                        <p><strong>{{appoinment.description}}</strong></p>
                                        <div class="modal-body">
                                            <p><strong>{{appoinment.user.id}}</strong></p>
                                            <p><strong>{{appoinment.user.userName}}</strong></p>
                                            <p><strong>{{appoinment.user.phoneNo}}</strong></p>
                                            <p><strong>{{appoinment.user.email}}</strong></p>
                                        </div>
                                        <%--<input id="dateValue2" name="date2" ng-model="date2" type="text" class="form-control" data-plugin="datepicker"/>--%>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </td>


                    <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button data-ng-init="onloadFun(appoinment)" ng-init="appoinment.Action = 'Approval'" ng-click="doApproval(appoinment)" ng-disabled="appoinment.disabled" class="btn btn-default btn-sm" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span>{{appoinment.Action}}</button></p></td>
                    <%-- <td><input id="btnAddProfile" type="button" class="btn btn-primary" value="Approval"/></td>--%>
                    <%--<td><button ng-click="doSomeStuff(appoinment)" ng-disabled="appoinment.disabled">Request</button></td>--%>

                    <%--<p>
                        <input id="id1" name="dateXX" ng-model="dateXX" type="text" class="form-control" data-plugin="datepicker"/>
                    </p>--%>

                    <td>

                        <button  id="editButton" class="btn btn-default btn-sm"
                                data-toggle="modal" data-target="#editAppointmentDetails{{$index}}" >Edit</button>

                        <div   class="modal fade" id="editAppointmentDetails{{$index}}" tabindex="-1" role="dialog" aria-labelledby="executionOptionLabel"
                               aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <br>
                                    <h3 align="center">Edit X Appoinment</h3>
                                    <div class="modal-body">

                                        <script type="text/javascript">
                                            $(function(){
                                                $("td.datepicker input:text").datepicker();

                                                // $("#time-pick-custom").timepicker('show').trigger();
                                                $('.time-pick-custom input:text').timepicker();
                                                $('.time-pick-custom input:text').on('click', function(){
                                                    $('.time-pick-custom').timepicker('show');
                                                });

                                            });

                                        </script>

                                        <div class="form-group" >
                                            <label>Appointment Type</label>
                                            <select ng-model="type" class="form-control" data-plugin="selectpicker" style="z-index:120000">
                                                <option>{{appoinment.type}}</option>

                                                <option>Hair Cut</option>
                                                <option>Hair Trim</option>
                                                <option>Bread Save</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Select Date and Time</label>
                                            <div class="input-date">

                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icon wb-calendar" aria-hidden="true"></i>
                                                    </span>

                                                    <table>
                                                        <tr>
                                                            <td class="datepicker">
                                                                <input id="date" name="date" type="text" class="form-control"  data-plugin="datepicker" autocomplete="off" value="{{appoinment.date}}"/>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <br/>

                                                <div class="input-group" id="disableTimeRangesExample">
                                                    <span class="input-group-addon">
                                                      <i class="icon wb-time" aria-hidden="true"></i>
                                                    </span>
                                                    <table>
                                                        <tr>
                                                            <td class="time-pick-custom">
                                                                <input id="time" name="time" type="text" class="form-control"  data-plugin="timepicker" autocomplete="off" value="{{appoinment.time}}"/>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea row="10" class="form-control" id="description" name="description" style="resize: none;">{{appoinment.description}}</textarea>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <button type="submit" class="col-md-2 col-sm-offset-10 btn btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <%--<div   class="modal fade" id="editAppointmentDetails{{$index}}" tabindex="-1" role="dialog" aria-labelledby="executionOptionLabel"
                               aria-hidden="true">

                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <br>
                                    <h3 align="center">Edit Appoinment</h3>
                                    <div class="modal-body">
                                        <script type="text/javascript">
                                            $(function(){
                                                $("td.datepicker input:text").datepicker();
                                                // $("#time-pick-custom").timepicker('show').trigger();
                                                $('.time-pick-custom input:text').timepicker();
                                                $('.time-pick-custom input:text').on('click', function(){
                                                    $('.time-pick-custom').timepicker('show');
                                                });

                                            });

                                        </script>

                                        <table class="datepicker">
                                            <tr>
                                                <td class="datepicker">
                                                    <input type="text" value="{{appoinment.date}}" />
                                                </td>
                                            </tr>
                                        </table>

                                        <table >
                                            <tr>
                                                <td class="time-pick-custom">
                                                    <input type="text" value="{{appoinment.time}}" />
                                                </td>
                                            </tr>
                                        </table>









                                        &lt;%&ndash;<table>

                                        <tr>
                                            <td><label for="program_name">Type</label></td>
                                            <td>
                                                <div class="form-group">

                                                    <select  ng-model="type" class="form-control" data-plugin="selectpicker">
                                                        <option>{{appoinment.type}}</option>
                                                        <option>Hair Cut</option>
                                                        <option>Hair Trim</option>
                                                        <option>Bread Save</option>
                                                    </select>
                                                </div>
                                            </td>
                                        <tr>
                                        <tr>
                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea row="10" class="form-control" id="description" name="description" style="resize: none;">{{appoinment.description}}</textarea>
                                            </div>
                                        </tr>
                                        <tr>
                                            <td><label for="program_name">Time</label></td>
                                            <td class="datepicker">
                                                <input type="text" value="{{appoinment.date}}" />
                                            </td>
                                        </tr>




                                    </table>&ndash;%&gt;
                                    </div>

                                </div>
                            </div>
                        </div>--%>
                    </td>



                    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-sm" data-title="Delete" data-toggle="modal" data-target="#delete" ng-click="deleteAppointment($index)"><span class="glyphicon glyphicon-trash"></span>Delete</button></p></td>
                </tr>
                </tbody>

            </table>
            <br/>

            <div style="float: right" class="pagination pagination-centered" ng-show="listAppoinment.length">
                <ul class="pagination-controle pagination">
                    <li>
                        <button type="button" class="btn btn-primary" ng-disabled="curPage == 0"
                                ng-click="curPage=curPage-1"> &lt;</button>
                    </li>
                    <li>
                        <button class="btn" readonly="true">{{curPage + 1}} of {{ numberOfPages() }}</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-primary"
                                ng-disabled="curPage >= listAppoinment.length/pageSize - 1"
                                ng-click="curPage = curPage+1">&gt;</button>
                    </li>
                </ul>
            </div>

        </div>

    </div>


    <%--USER DETAILS--%>

    <div class="panel" id="userid" style="display:none">
        <div ng-controller="allCustomerDetails" class="panel-body" ng-init="showData()">
            <header class="panel-heading">
                <div class="row">

                    <div class="col-md-4">
                        <h4 class="panel-title">Customer Details <span class="tooltip-dark" data-toggle="tooltip" data-original-title="Help tips" data-trigger="focus / hover"><button type="button" class="btn-info popover-info btn-sm btn-icon btn-inverse btn-info btn-round" data-content="Can accept or reject any applications for selection test in this table. And can search any content of the table. And also can view fully application through the id." data-toggle="popover" data-original-title="About this table..." data-trigger="click"><i class="grey-500 icon wb-info"></i></button></span></h4>
                    </div>
                    <div class="col-md-8" >
                        <br/>
                        <table>
                            <td>
                                <div class="col-md-12">
                                    <input class="form-control" type="text" ng-model="searchText" placeholder="Search by anything">
                                </div>
                            </td>
                            <td>
                                <div class="col-md-12">
                                    <a href="main2"><input type="button" class="btn btn-primary" value="Add Customer"/></a>
                                </div>
                            </td>

                        </table>
                    </div>

                </div>
            </header>
            <div style="text-align: center">


            </div>
            <table  class="table table-hover dataTable table-striped width-full table-bordered">
                <thead>
                <th>UserId</th>
                <th>UserName</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Edit</th>
                <th>Delete</th>
                </thead>

                <tr ng-repeat="customer in listCustomers | filter:searchText | pagination: curPage * pageSize | limitTo: pageSize">

                    <td >{{$index + 1 }}</td>
                    <td >{{customer.userName}}</td>
                    <td >{{customer.email}}</td>
                    <td >{{customer.phoneNo}}</td>

                    <td >

                        <button type="button" class="btn btn-default btn-xs"
                                data-toggle="modal" data-target="#editCustomer{{$index}}" ><span class="glyphicon glyphicon-trash"></span>Edit</button>

                        <div   class="modal fade" id="editCustomer{{$index}}" tabindex="-1" role="dialog" aria-labelledby="executionOptionLabel"
                               aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <br>
                                    <h3 align="center">Edit Customer</h3>
                                    <div class="modal-body">



                                        <div class="form-group">
                                            <label>Customer Name</label>
                                            <input class="form-control" id="cusname" name="cusname" value="{{customer.userName}}"></input>
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" id="email" name="email" value="{{customer.email}}"></input>
                                        </div>
                                        <div class="form-group">
                                            <label>Phone No</label>
                                            <input class="form-control" id="phoneNo" name="phoneNo" value="{{customer.phoneNo}}"></input>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <button type="submit" class="col-md-2 col-sm-offset-10 btn btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </td>


                    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ng-click="deleteCustomer($index)"><span class="glyphicon glyphicon-trash"></span>Delete</button></p></td>
                </tr>
                </tbody>

            </table>
            <br/>

            <div style="float: right" class="pagination pagination-centered" ng-show="listCustomers.length">
                <ul class="pagination-controle pagination">
                    <li>
                        <button type="button" class="btn btn-primary" ng-disabled="curPage == 0"
                                ng-click="curPage=curPage-1"> &lt;</button>
                    </li>
                    <li>
                        <button class="btn" readonly="true">{{curPage + 1}} of {{ numberOfPages() }}</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-primary"
                                ng-disabled="curPage >= listCustomers.length/pageSize - 1"
                                ng-click="curPage = curPage+1">&gt;</button>
                    </li>
                </ul>
            </div>

        </div>

    </div>

    <%--ADD APPOINMENT--%>

    <div class="panel" id="add_appoinment" style="display:none">

        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h3 class="panel-title">Add a new appointment</h3>
        </header>
        <div ng-controller="allAppoinmentDetails as ctrl" class="panel-body">

            <form ng-submit="submitAppoinmentForm()" id="form1">



                <div class="form-group">
                    <label for="program_name">Appointment Type</label>
                    <select  ng-model="type" class="form-control"  required blur="submitToggle" data-plugin="selectpicker">
                        <option>Hair Cut</option>
                        <option>Hair Trim</option>
                        <option>Bread Save</option>
                    </select>
                </div>


                <%--WorkedOne--%>
                <%--<input id="date-birth" class="form-control" type="date" ng-model="date">--%>

                <div class="form-group">
                    <label for="starting_date">Select Date and Time</label>
                    <div class="input-date">
                        <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>

                            <input id="dateValue" name="date" ng-model="date" type="text" required blur="submitToggle" class="form-control" data-plugin="datepicker"/>

                        </div>
                        <br/>
                        <div class="input-group" id="disableTimeRangesExample">
                                <span class="input-group-addon">
                                  <i class="icon wb-time" aria-hidden="true"></i>
                                </span>
                            <%--<input name="time" ng-model="time" type="time" class="form-control" autocomplete="off"/>--%>
                            <input id="timeValue" name="time" ng-model="time" type="text" required blur="submitToggle" class="form-control"  data-plugin="timepicker" autocomplete="off"/>
                        </div>
                    </div>


                    <%--
                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="icon wb-calendar" aria-hidden="true"></i>
                                            </span>
                        <input type="text" class="form-control" data-format="yyyy-mm-dd" data-plugin="datepicker" name="starting_date" id="fld_dob" placeholder="YYYY-MM-DD">
                    </div>--%>
                </div>

                <div class="form-group">
                    <label for="num_years">Description</label>
                    <textarea ng-model="description" row="10" class="form-control" id="description" name="description" style="resize: none;"></textarea>
                </div>

                <div class="row">
                    <div class="col-md-10">
                        <table>
                            <tr>
                                <div class="form-group">
                                    <button type="submit" ng-click="alertdata()" onClick="window.location.reload()" class="col-md-2 col-sm-offset-10 btn btn-primary">Save</button>
                                </div>
                            </tr>
                            <tr>
                                <div class="col-md-12">
                                    <a href="main"><input  type="button" class="btn btn-primary" value="Back"/></a>
                                </div>
                            </tr>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>


</div>


<!-- End Page -->

<!-- Footer -->
<%--<%@include file="includes/includejs.jsp" %>--%>
<script type="text/javascript">
    $(document).ready(function () {
        $("#editButton").click(function () {
            alert('EDIT MODE ON');
        });
    });
</script>


<script>
    $('#check-out').datepicker();
</script>

<script>
    (function (document, window, $) {
        'use strict';

        var Site = window.Site;

        $(document).ready(function ($) {
            Site.run();
        });

    })(document, window, jQuery);

    $("#addAppoinment").click(function () {
        resetOther();
        $('#appoid').hide();
        $('#userid').hide();
        $('#add_appoinment').show();
    });

    function resetOther() {
        $('.pages-custom').hide();
    }

</script>

</body>

</html>