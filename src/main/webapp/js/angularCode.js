var app = angular.module('app', []);

app.controller('userRegController', function($scope, $http, $location) {

    $scope.passwordConf = null;
    $scope.usrnameConf = null;

    $scope.submitForm = function(){
        if($scope.passwordConf == "matched" && $scope.usrnameConf == "notexist"){
            var url = $location.absUrl() + "/postcustomer";

            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            }

            var data = {
                userName: $scope.username,
                email: $scope.email,
                phoneNo: $scope.phoneno,
                password: $scope.password
            };

            $http.post(url, data, config).then(function (response) {
                $scope.postResultMessage = "Sucessful!";
            }, function (response) {
                $scope.postResultMessage = "Fail!";
            });

            $scope.username = "";
            $scope.email = "";
            $scope.phoneno ="";
            $scope.password ="";
            $scope.passwordsignup_confirm ="";
            $scope.passwordConfMsg ="";
        }
        else{
            $scope.passwordConfMsg = "Please enter correct values";
        }

    }

    $scope.usernameValidation = function () {

        if($scope.username.length>4){
            var url = $location.absUrl() + "/getUserByUserName/" + $scope.username;

            var config = {
                headers : {
                    'Accept': 'text/plain'
                }
            }
            $http.get(url,config).then(function (response) {
                $scope.response = response.data
                //alert($scope.response);
                if($scope.response=="exist"){
                    $scope.result = "Username already exist!";
                    $scope.usrnameConf = "exist";
                }
                else{
                    $scope.result = '';
                    $scope.usrnameConf = "notexist";
                }

            }, function error(response) {
                $scope.postResultMessage = "Error with status: " +  response.data;
                alert($scope.postResultMessage);
            });
        }
        else{
            $scope.result = "Username should be more than 4 characters";
        }

    }

    $scope.passwordConfirmation = function (password) {
        /*alert($scope.passwordsignup_confirm);*/

        if($scope.passwordsignup_confirm==password){
            $scope.disabled = false;
            $scope.passwordConfMsg = "Passwords matched";
            $scope.passwordConf = "matched";

        }
        else{
            $scope.disabled = true;
            $scope.passwordConfMsg = "Password not matched";
            $scope.passwordConf = "notmatched";

        }

    }

    $scope.updatePassword = function (userIdVal) {

        if($scope.passwordConf == "matched"){

            var url = $location.absUrl() + "/"+userIdVal+"/"+$scope.password;

            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            }


            $http.post(url, config).then(function (response) {
                $scope.postResultMessage = "Sucessful!";
                alert($scope.postResultMessage);
                window.location.href = 'http://localhost:9092/beautysalon/login';

            }, function (response) {
                $scope.postResultMessage = "Fail!";
            });
        }
    }

});

app.controller('forgetPassword',function($scope, $http, $location){


    $scope.sendEmail = function() {

        $scope.result = "We sending details your gmail....";

        var url = $location.absUrl() + "/sendMail/" + $scope.username;


        var config = {
            headers: {
                'Accept': 'text/plain'
            }
        }
        $http.get(url, config).then(function (response) {
            $scope.result = response.data
            $scope.username = "";


        }, function error(response) {
            $scope.postResultMessage = "Error with status: " + response.data;

        });

    }
    $scope.errorMsg = function () {
        $scope.result = null;

    }

});

app.controller('postAppoinmentCtr', function($scope, $http, $location) {

    $scope.submitForm = function(){

        var url = $location.absUrl() + "/addAppoinment";

        var dateVal = document.getElementById("dateValue").value;
        var timeVal = document.getElementById("timeValue").value;

        $scope.date = dateVal;
        $scope.time = timeVal;


        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;'
            }
        }

        var data = {
            type: $scope.type,
            date: $scope.date,
            time: $scope.time,
            description: $scope.description
        };


        $http.post(url, data, config).then(function (response) {
            $scope.postResultMessage = "Sucessful!";
        }, function (response) {
            $scope.postResultMessage = "Fail!";
        });

        $scope.type = "";
        $scope.date = "";
        $scope.time ="";
        $scope.description ="";



    }
});



/*Get ALL Customer Details*/
app.controller('allCustomerDetails', function($scope, $http, $location) {
    $scope.listCustomers = [];

    // for deleted message
    $scope.error=false;
    $scope.restMessage;

    //alert($scope.id);
    // $scope.getAllCustomer =
    function getAllCustomer(){
        // get URL
        var url = $location.absUrl() + "/allCustomerDetails";
        //alert(url);
        var config = {
            headers : {
                'Content-Type' : 'application/json;charset=utf-8;'
            }
        }

        $http.get(url, config).then(function(response) {
            $scope.getDivAvailable = true;
            $scope.listCustomers = response.data;

        }, function error(response) {
            $scope.postResultMessage = "Error Status: " +  response.statusText;
        });
    }
    getAllCustomer();

    $scope.deleteCustomer= function(index){

        var txt;
        var r = confirm("Press a button!");
        if (r == true) {
            txt = "You pressed OK!";

            var url = $location.absUrl() + "/deleteUser/"+ $scope.listCustomers[index].id;

            var config = {
                headers : {
                    'Accept': 'text/plain'
                }
            }
            // delete processing
            $http.delete(url, config).then(function (response) {
                $scope.error = false;
                console.log(response.data);
                $scope.restMessage = "Deleted an customer: " + $scope.listCustomers[index].name + "!";

                // remove item on angularjs model
                $scope.listCustomers.splice(index, 1);

            }, function error(response) {
                $scope.error = true;
                $scope.restMessage = "Error!";
            });
        } else {
            txt = "You pressed Cancel!";
        }

    };

    $scope.showData = function( ) {

        $scope.curPage = 0;
        $scope.pageSize = 5;
        $scope.numberOfPages = function () {
            return Math.ceil($scope.listCustomers.length / $scope.pageSize);
        };



    }



});

/*Get all appoinment details by user id*/
app.controller('allAppoinmentDetails', function($scope, $http, $location) {

    $scope.listAppoinment = [];
    $scope.msg = null;
    // $scope.getAllCustomer =
    function getAllAppoinment(){
        // get URL

        if($scope.msg =="pending"){
            var url = $location.absUrl() + "/getAllPendingAppoinmentDetails/"+"pending";

        }
        else if($scope.msg =="approved"){
            var url = $location.absUrl() + "/getAllPendingAppoinmentDetails/"+"approved";

        }
        else{
            var url = $location.absUrl() + "/getAllAppoinmentDetails";


        }

        var config = {
            headers : {
                'Content-Type' : 'application/json;charset=utf-8;'
            }
        }
        // do getting
        /*$http.get(url).then(function (response) {
            $scope.getDivAvailable = true;
            $scope.listCustomers = response.data;
        }, function error(response) {
            $scope.postResultMessage = "Error Status: " +  response.statusText;
        });*/

        $http.get(url, config).then(function(response) {
            $scope.getDivAvailable = true;
            $scope.listAppoinment = response.data;

            $scope.onloadFun = function (appoinment) {

                if(appoinment.approval=="1"){
                    appoinment.Action = "Approved"
                    appoinment.disabled = true;
                }
                else{
                    appoinment.Action = "Approval"
                    appoinment.disabled = false;
                }

            }

        }, function error(response) {
            $scope.postResultMessage = "Error Status: " +  response.statusText;
        });
        alert('Alert Function method');


    }
    $scope.clear = function(){
        student = {type:"",date : "", time : "", description : ""};
    }
    $scope.index = 0;

    getAllAppoinment();



    /*pagination part-01 showData start*/
    $scope.showData = function( ) {

        $scope.curPage = 0;
        $scope.pageSize = 5;
        $scope.numberOfPages = function () {
            return Math.ceil($scope.listAppoinment.length / $scope.pageSize);
        };

        $scope.appoinmentTypes = [
            'Hair Cut',
            'Hair Trim',
            'Bread Save'
        ];



    }
    //Edit Appointment
    $scope.submitForm = function(appoinment, index){
        var url = $location.absUrl() + "/editAppoinment/"+appoinment.id;

        alert(appoinment.id);

        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;'
            }
        }

        var data = {
            id: appoinment.id,
            type: appoinment.type,
            date: appoinment.date,
            time: appoinment.time,
            description: appoinment.description
        };

        /*$location.reload();*/

        /*alert(id + " " + type + " " + $scope.date + " " + time + " " + description);*/

        $http.put(url, data, config).then(function (response) {

            /*getAllCustomer();*/
            // alert("okay bnbn jags " + index);


            // $(".modal").click();
            // $("#editAppointmentDetails1 .close").click();
            $('#editAppointmentDetails'+index).modal('toggle');
            // $('.sad').modal().hide();

            $scope.putResultMessage = "Sucessful!";
        }, function (response) {
            $scope.putResultMessage = "Fail!";
        });

        /*$window.location.reload();*/

        /*$scope.type = "";
        $scope.date = "";
        $scope.time = "";
        $scope.description = "";*/

    }
    /*pagination part-01 showData end*/

    $scope.doApproval = function(appoinment) {
        //In this scenario do appointment approval

        var url = $location.absUrl() + "/doApproval/"+appoinment.id;
        var config = {
            headers : {
                'Content-Type' : 'application/json;charset=utf-8;'
            }
        }
        $http.put(url, config).then(function(response) {
            appoinment.Action = "Approved"
            appoinment.disabled = true;

        }, function error(response) {
            $scope.postResultMessage = "Error Status: " +  response.statusText;
        });
        /* appoinment.Action = "Approved"
         appoinment.disabled = true;*/

    }

    $scope.pendingAction = function (msg) {
        $scope.msg = msg;
        getAllAppoinment();
    }


    $scope.deleteAppointment= function(index){

        var txt;
        var r = confirm("Press a button!");
        if (r == true) {
            txt = "You pressed OK!";

            var url = $location.absUrl() + "/deleteAppointment/"+ $scope.listAppoinment[index].id;

            var config = {
                headers : {
                    'Accept': 'text/plain'
                }
            }
            // delete processing
            $http.delete(url, config).then(function (response) {
                $scope.error = false;
                console.log(response.data);
                $scope.restMessage = "Deleted an customer: " + $scope.listAppoinment[index].name + "!";

                // remove item on angularjs model
                $scope.listAppoinment.splice(index, 1);

            }, function error(response) {
                $scope.error = true;
                $scope.restMessage = "Error!";
            });
        } else {
            txt = "You pressed Cancel!";
        }

    };

    $scope.submitAppoinmentForm = function(){


        var url = $location.absUrl() + "/addAppoinment";

        var dateVal = document.getElementById("dateValue").value;
        var timeVal = document.getElementById("timeValue").value;

        $scope.date = dateVal;
        $scope.time = timeVal;

        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;'
            }
        }

        var data = {
            type: $scope.type,
            date: $scope.date,
            time: $scope.time,
            description: $scope.description
        };


        $http.post(url, data, config).then(function (response) {
            $scope.postResultMessage = "Sucessful!";
        }, function (response) {
            $scope.postResultMessage = "Fail!";
        });

        $scope.type = "";
        $scope.date = "";
        $scope.time ="";
        $scope.description ="";






    }
    var scope = $scope;

    scope.alertchilddata = function(){
        alert('HI BABES');
    }
    scope.alertdata = function(){
        alert('Parent Alert');
        //getAllAppoinment();
    }



});



/*Pagination part-02 start*/
angular.module('app').filter('pagination', function()
{
    return function(input, start)
    {
        start = +start;
        return input.slice(start);
    };
});
/*Pagination part-02 end*/


//myApp.directive('myDirective', function() {});
//myApp.factory('myService', function() {});




/*app.controller('getallcustomersController', function($scope, $http, $location) {
    /!* alert("Hi");*!/
    $scope.showAllCustomers = false;

    $scope.getAllCustomers = function() {
        var url = $location.absUrl() + "/allCustomerDetails";
        /!*alert(url);
        alert("hi");*!/
        var config = {
            headers : {
                'Content-Type' : 'application/json;charset=utf-8;'
            }
        }

        $http.get(url, config).then(function(response) {


            if (response.data.status == "Done") {
                $scope.allcustomers = response.data;

                $scope.showAllCustomers = true;
            } else {
                $scope.getResultMessage = "get All Customers Data Error!";
            }

        }, function(response) {

            $scope.getResultMessage = "Fail!";
        });
        /!* alert("Hi3");*!/
    }
});

app.controller('getcustomerController', function($scope, $http, $location) {

    $scope.showCustomer = false;

    $scope.getCustomer = function() {
        var url = $location.absUrl() + "customer/" + $scope.customerId;

        var config = {
            headers : {
                'Content-Type' : 'application/json;charset=utf-8;'
            }
        }

        $http.get(url, config).then(function(response) {

            if (response.data.status == "Done") {
                $scope.customer = response.data;
                $scope.showCustomer = true;

            } else {
                $scope.getResultMessage = "Customer Data Error!";
            }

        }, function(response) {
            $scope.getResultMessage = "Fail!";
        });

    }
});

app.controller('getcustomersbylastnameController', function($scope, $http, $location) {

    $scope.showCustomersByLastName = false;

    $scope.getCustomersByLastName = function() {
        var url = $location.absUrl() + "findbylastname";

        var config = {
            headers : {	'Content-Type' : 'application/json;charset=utf-8;' },

            params: { 'lastName' : $scope.customerLastName }
        }

        $http.get(url, config).then(function(response) {

            if (response.data.status == "Done") {
                $scope.allcustomersbylastname = response.data;
                $scope.showCustomersByLastName = true;

            } else {
                $scope.getResultMessage = "Customer Data Error!";
            }

        }, function(response) {
            $scope.getResultMessage = "Fail!";
        });

    }
});*/

