$(function () {
    $("#submitUserForm").submit(function (e) {
        e.preventDefault();
        var frm = $("#submitUserForm");
        var data = {};
        $.each(this,function (i,v) {
            var input = $(v);
            data[input.attr("name")] = input.val();
            delete data["undefined"];
        });
        saveRequestedData(frm,data,"user");
    });

});

function  saveRequestedData(frm,data,type) {

    $.ajax({
        contentType : "application/json; charset=utf-8",
        async: false,
        type : frm.attr("method"),
        url : frm.attr("action"),
        dataType : 'json',
        data : JSON.stringify(data),
        /*beforeSend: function(xhr) {
            // here it is
            xhr.setRequestHeader(header, token);
        },*/
        success : function(data){
            toastr.success(data.message,data.title,{
                closeButton : true
            });
            //fetchList(type);
        }
    });
}

