<%--
  Created by IntelliJ IDEA.
  User: Matrix4F
  Date: 5/30/2018
  Time: 12:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Spring Boot Example</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script
            src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script>
    <script src="js/test/test.js"></script>
    <link rel="stylesheet"
          href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" />
</head>
<body>
<div class="container" ng-app="app">
    <h1>Test Project</h1>

    <div class="row">
        <div ng-controller="postController" class="col-md-3">
            <form name="customerForm" ng-submit="submitForm()">
                <label>Name</label>
                <input type="text" name="name"	class="form-control" ng-model="name" />
                <label>Email</label>
                <input type="text" name="email" class="form-control" ng-model="email" />
                <label>ContactNo</label>
                <input type="text" name="contactNo" class="form-control" ng-model="contactNo" />
                <label>Company Name</label>
                <input type="text" name="companyName" class="form-control" ng-model="companyName" />
                <label>Description</label>
                <input type="text" name="description" class="form-control" ng-model="description" />

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <p>{{postResultMessage}}</p>
        </div>
    </div>
</div>
</body>
</html>
