var app = angular.module('app', []);

app.controller('postController', function($scope, $http, $location) {
    $scope.submitForm = function(){
        var url = $location.absUrl() + "addCustomer";

        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;'
            }
        }

        var data = {
            name: $scope.name,
            email: $scope.email,
            contactNo: $scope.contactNo,
            companyName:$scope.companyName,
            description:$scope.description

        };


        $http.post(url, data, config).then(function (response) {
            $scope.postResultMessage = "Sucessful!";
        }, function (response) {
            $scope.postResultMessage = "Fail!";
        });

        $scope.name= "";
        $scope.email= "";
        $scope.contactNo= "";
        $scope.companyName= "";
        $scope.description= "";
    }
});